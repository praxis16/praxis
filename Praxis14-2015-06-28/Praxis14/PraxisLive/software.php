<?php
session_start();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets2/css/style.css">
    <link rel="shortcut icon" href="favicon.ico">        
 <link rel="stylesheet" href="assets/css/headers/header1.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="assets/css/style_responsive.css" />
	   <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" />           
    <link rel="stylesheet" href="assets/css/effects.css" />    
   <link rel="stylesheet" href="assets/plugins/revolution_slider/rs-plugin/css/settings.css">
    <!-- CSS Implementing Plugins -->    
    <link rel="stylesheet" href="assets2/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets2/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="assets2/plugins/revolution_slider/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="assets2/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets2/css/themes/default.css" id="style_color">
    <link rel="stylesheet" href="assets2/css/themes/headers/default.css" id="style_color-header-1">    
<style>
.name_tab
{
color:green;
font-weight:bold;
font-family:'Open Sans', sans-serif; 
}
</style>
    <title>Praxis '14 | Events</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->


    <!-- CSS Global Compulsory -->

</head> 

<body style="background-color:white;">
 <div class="top">
    <div class="container">         
        <ul class="loginbar pull-right">
<li id='name_tab' class='name_tab'></li>
<li class="devider" id='dividern'></li> 
         <li id='reg_tab'><a href="Register.html" class="login-btn">Register</a></li>   
            <li class="devider"></li>
 <li id="uniqueid_tab" class="name_tab"></li>   
            <li class="devider">&nbsp;</li>
<li id="event_tab" class="event_tab"></li>  
            <li class="devider">&nbsp;</li>
            <li id="login_tab"><a href="Login.php" class="login-btn">Login</a></li> 
        </ul>
</div>      
</div><!--/top-->
<?php
require('db_connect.php');

if(isset($_SESSION['userid']))
{

echo "<script>document.getElementById('login_tab').innerHTML='<a href=\'Logout.php\'>Logout</a>'</script>";

echo "<script>document.getElementById('event_tab').innerHTML='<a href=\"myEvents.php\">My Events</a>'</script>";

echo "<script>document.getElementById('reg_tab').style.color=\"#2B3856\";</script>";

echo "<script>document.getElementById('reg_tab').style.fontWeight=\"bold\";</script>";

echo "<script>document.getElementById('reg_tab').style.fontFamily='\'Open Sans\', sans-serif'</script>";

echo "<script>document.getElementById('reg_tab').innerHTML='".$_SESSION['userid']."'</script>";


$query="Select * from confirmedemails where email='".$_SESSION['userid']."'";

$result=mysql_query($query) or die("Could not execute query");

while($row=mysql_fetch_array($result))
{
echo "<script>document.getElementById('name_tab').innerHTML='<img src=\'assets/img/user.png\' height=\'28px\' width=\'28px\'><img>".$row['fname']." ".$row['lname']."'</script>";
echo "<script>document.getElementById('dividern').innerHTML='&nbsp;'</script>";
echo "<script>document.getElementById('uniqueid_tab').innerHTML='".$row['uniqueid']."'</script>";
//echo $row['uniqueid'];
}
}

?>



    <!--=== Header ===-->    
    <div class="header">
    <div class="container"> 
        <!-- Logo -->       
        <div class="logo">                                             
            <a href="index.php"><img src="assets2/img/logo.jpg" style="padding-right:53px"></a>
        </div><!-- /logo -->        
                                 
       
    
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                     <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
      
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse nav-collapse">
                    <ul class="nav navbar-nav">
                 <li><a href="index.php">Home</a></li>
                    
                        
                        <li class="dropdown active">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                Events
                            </a>
                            <ul class="dropdown-menu">
                              
                               
                                <li><a href="software.php">Software</a></li>
                                <li><a href="electronics.php">Electronics</a></li>
                                <li><a href="robotics.php">Robotics</a></li>
                <li><a href="gaming.php">Gaming</a></li>
                                <li><a href="funevents.php">Miscellaneous</a></li>

                            </ul>
                        </li>
                        <li><a href="AboutUs.php">About Us</a></li>
                        <li><a href="Praxis13.php">Praxis '13</a></li>
                        <li><a href="Spo14.php">Sponsors</a></li>
                        
                    </ul>
                </div><!--/navbar-collapse-->
            </div>    
        </div>   
<br>    
<br><br>    
        <!-- End Navbar -->
    </div>
    </div>
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Events under this Category</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li><a href="">Events</a></li>
                <li class="active">Software</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->
<br/>
<br/>
    <!--=== Content Part ===-->     
<div class="container">     
        <div class="row-fluid">
            <div class="row-fluid margin-bottom-40">
                    <?php
                        $i=1;
                        $j=0;                        
                        $result = mysql_query("SELECT * FROM events WHERE category='Software'") or die(mysql_error());
                        while($row = mysql_fetch_array($result ) ){
                            if($j==0)
                                echo '<ul class="thumbnails" style="list-style:none;">';
                            echo "<li class='span4'>";
                            echo "<div class='thumbnail thumbnail-kenburn'>";
                            echo "<div class='overflow-hidden'> <a class='fancybox' href='#inline".$i."' title='".$row['category']."''><img src='".$row['url']."' alt=''/></a></div>";
                            echo "<h4 class='text-center'><strong>".$row['eventname']."</strong></h4>";
                            echo "<p class='text-center'><a class='btn-u btn-u-small fancybox' href='#inline".$i."' title='".$row['category']."''>View Details</a></p>";
                            echo "</div>";
                            echo "</li>";
                            $i++;
                            $j++;
                            if($j==3)
                            {
                                $j=0;
                                echo '</ul>';
                            }
                        }
                        if($j!=0)
                                echo '</ul>';
                    ?>            
            </div>

            <?php
                $i=1;
                $result = mysql_query("SELECT * FROM events WHERE category='Software'") or die(mysql_error());
                while($row = mysql_fetch_array($result ) ){
                    echo "<div id='inline".$i."' style='display: none;max-width:600px;' >";
                    echo "<div class='thumbnail thumbnail-kenburn'><img src='".$row['url']."' alt='' style='margin-left:auto;margin-right:auto;'/></div>";
                    echo "<h4><strong>".$row['eventname']."</strong></h4>";
                    echo "<p>".$row['descr']."</p>";
                    echo "<p>Participate in teams of ".$row['teams']."</p>";
                    echo "<h5><strong>Rs. ".$row['fees']."</strong></h5>";
                    echo "<span><strong>Prizes:</strong></span>";
                    if($row['pm_1'] != '-')
                        echo "<p>1st prize : Rs.".$row['pm_1']."</p>";
                    if($row['pm_2'] != '-')
                        echo "<p>2nd prize : Rs.".$row['pm_2']."</p>";
                    if($row['pm_3'] != '-')
                        echo "<p>3rd prize : Rs.".$row['pm_3']."</p>";
                    echo "<p><a href='evntreg.php?evtnm=".$row['eventname']."' class='btn-u'>Register</a></p>";
                    echo "</div>";
                    $i++; 
                }
            ?>
        </div>
    </div>
    <!--=== End Content Part ===-->

    <!--=== Footer ===-->
    <div class="footer">    <div class="container">        <div class="row-fluid">            <div class="span4">                <!-- About -->                <div class="headline"><h3>About</h3></div>                  <p class="margin-bottom-25">Praxis is Vivekanand Education Society's Institute of Technology's annual technical festival.</p><br><img style="height:170px;"src="assets/img/ves_logo.png" />            </div>            <div class="span4">                <div class="posts">                    <div class="headline"><h3>Recent Tech Feeds</h3></div>                       <script language="JavaScript" src="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y"  charset="UTF-8" type="text/javascript"></script><noscript><a href="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y&html=y">View RSS feed</a></noscript>                </div>            </div><!--/span4-->            <div class="span4">                <div class="headline"><h3>Contact Us</h3></div>                 <address>                    Vivekanand Education Society's Institute of Technology<br /> Collector Colony,Chembur  <br />                                   </address> 
Phone: +91 022 61532532<br/><br/>
Official Website:<a href="http://vesit.edu">vesit.edu</a>
               <!-- Stay Connected -->                


 <div class="headline"><h2>Stay Connected</h2></div> 
                      <a target="_blank" href="https://www.facebook.com/Vesit.praxis"><img src="assets/img/facebook-logo.png" style="margin-top:0%;width:10%;height:10%"/></a>
                    <!-- End Social Links -->
                </div>

<!--/span4-->        </div><!--/row-fluid-->     </div><!--/container--> </div><!--/footer-->    <!--=== End Footer ===--><!--=== Copyright ===--><div class="copyright">    <div class="container">        <div class="row-fluid">            <div class="span8">                                     <p>2014 &copy; Praxis. ALL Rights Reserved.</p>            </div>                </div><!--/row-fluid-->    </div><!--/container--> </div><!--/copyright--> <!--=== End Copyright ===-->

<!--=== Copyright ===-->

<!--=== End Copyright ===-->

</div><!--/wrapper-->

<?php
    mysql_close();
?>

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="assets2/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets2/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets2/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets2/plugins/back-to-top.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets2/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>
<!--[if lt IE 9]>
    <script src="assets2/plugins/respond.js"></script>
<![endif]-->


<script type="text/javascript">
    $(document).ready(function() {
        /*
         *  Simple image gallery. Uses default settings
         */

        $('.fancybox').fancybox();

        /*
         *  Different effects
         */

        // Change title type, overlay closing speed
        $(".fancybox-effects-a").fancybox({
            helpers: {
                title : {
                    type : 'outside'
                },
                overlay : {
                    speedOut : 0
                }
            }
        });

        // Disable opening and closing animations, change title type
        $(".fancybox-effects-b").fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            helpers : {
                title : {
                    type : 'over'
                }
            }
        });

        // Set custom style, close if clicked, change title type and overlay color
        $(".fancybox-effects-c").fancybox({
            wrapCSS    : 'fancybox-custom',
            closeClick : true,

            openEffect : 'none',

            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });

        // Remove padding, set opening and closing animations, close if clicked and disable overlay
        $(".fancybox-effects-d").fancybox({
            padding: 0,

            openEffect : 'elastic',
            openSpeed  : 150,

            closeEffect : 'elastic',
            closeSpeed  : 150,

            closeClick : true,

            helpers : {
                overlay : null
            }
        });

        /*
         *  Button helper. Disable animations, hide close button, change title type and content
         */

        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,

            helpers : {
                title : {
                    type : 'inside'
                },
                buttons : {}
            },

            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });


        /*
         *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
         */

        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,
            arrows    : false,
            nextClick : true,

            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });

        /*
         *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
        */
        $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect : 'none',
                closeEffect : 'none',
                prevEffect : 'none',
                nextEffect : 'none',

                arrows : false,
                helpers : {
                    media : {},
                    buttons : {}
                }
            });

        /*
         *  Open manually
         */

        $("#fancybox-manual-a").click(function() {
            $.fancybox.open('1_b.jpg');
        });

        $("#fancybox-manual-b").click(function() {
            $.fancybox.open({
                href : 'iframe.html',
                type : 'iframe',
                padding : 5
            });
        });

        $("#fancybox-manual-c").click(function() {
            $.fancybox.open([
                {
                    href : '1_b.jpg',
                    title : 'My title'
                }, {
                    href : '2_b.jpg',
                    title : '2nd title'
                }, {
                    href : '3_b.jpg'
                }
            ], {
                helpers : {
                    thumbs : {
                        width: 75,
                        height: 50
                    }
                }
            });
        });


    });
</script>

</body>
</html>