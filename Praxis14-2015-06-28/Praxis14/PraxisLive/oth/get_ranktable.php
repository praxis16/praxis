<?php
session_start();
if(!isset($_SESSION['userid'])) {
  header('location: Login.php');
}
?>
<html>
<head>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets2/css/style.css">
<style type="text/css">
	.table-hover > tbody > tr:hover > td, .table-hover > tbody > tr:hover > th {

		background-color:lightsteelblue;

	}
  body::-webkit-scrollbar {
    width: 15px;
}

body::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    border-radius: 10px;
}

body::-webkit-scrollbar-thumb {
    border-radius: 10px;
    height: 30px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.7);
}
</style>
</head>
<?php
require('db_connect2.php');
$query="SELECT email,rank,player_name,rank_change from oth_playerdata order by rank asc";
$result=mysqli_query($connection, $query) or die('a');

			echo"<table class='table table-hover' style='width:100%; background: #343c55; color: white'>";
                   echo"<thead><tr><th>Rank<th>Player Name<th>Rank Change</tr></thead>";
echo"<tbody>";
while($row=mysqli_fetch_array($result, MYSQLI_ASSOC))
{
if($row['email']==$_SESSION['userid'])
{
echo "<tr style='background-color:steelblue;color:white;'>";
}
else
{
echo "<tr>";
}

echo "<td>".$row['rank'];
echo "<td>".$row['player_name'];
if($row['rank_change']<0)
{

echo "<td style='text-align:center'><img src='down_arrow.png' height='15px' width='15px'></img>".abs((int)$row['rank_change']);
}
if($row['rank_change']>0)
{
echo "<td style='text-align:center'><img src='up_arrow.png' height='15px' width='15px'></img>".$row['rank_change'];
}
if($row['rank_change']==0)
{
echo "<td style='text-align:center'>".$row['rank_change'];
}
echo "</tr>";
 }
echo"</tbody>";
echo"</table>";
mysqli_close($connection);
?>
</html>
