<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php
  session_start();
  if(isset($_SESSION['userid'])) {
    header('location: oth.php');
  }
?>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets2/css/style.css">
    <link rel="shortcut icon" href="favicon.ico">
 <link rel="stylesheet" href="assets/css/headers/header1.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="assets/css/style_responsive.css" />
	   <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" />
    <link rel="stylesheet" href="assets/css/effects.css" />
   <link rel="stylesheet" href="assets/plugins/revolution_slider/rs-plugin/css/settings.css">
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets2/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets2/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="assets2/plugins/revolution_slider/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="assets2/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets2/css/themes/default.css" id="style_color">
    <link rel="stylesheet" href="assets2/css/themes/headers/default.css" id="style_color-header-1">
<style>
.name_tab
{
color:green;
font-weight:bold;
font-family:'Open Sans', sans-serif;
}
.warning
{
color:red;
margin-top:-8px;
}
</style>


<script>
var submitted=0;
var emailOk=1;
var emailvalid=0;
var email;
function settrim()
{
alert("/"+document.getElementById("email").trim()+"/");
}
function incr()
{
submitted++;
//alert(submitted);
}

function checkcall()
{
if(submitted!=0)
{
validate();
check_confirmed(document.getElementById('email').value);
}
}

function validate()
{
document.getElementById('fnamew').innerHTML="";
document.getElementById('lnamew').innerHTML="";
document.getElementById('emailw').innerHTML="";
document.getElementById('contactw').innerHTML="";
document.getElementById('passw').innerHTML="";
document.getElementById('confpassw').innerHTML="";
document.getElementById( 'acceptw').innerHTML="";
document.getElementById('match').innerHTML="";
document.getElementById('match').style.color="red";

var error=0;
//alert(error);
if(document.getElementById('fname').value=="")
{
document.getElementById('fnamew').innerHTML="first name required";
error++;
}
//alert(error);
if(document.getElementById('lname').value=="")
{
document.getElementById('lnamew').innerHTML="last name required";
error++;
}
//alert(error);
if(document.getElementById('email').value=="")
{
document.getElementById('emailw').innerHTML="email address required";
error++;
}
//alert(error);
if(document.getElementById('contact').value=="")
{
document.getElementById('contactw').innerHTML="mobile number required";
error++;
}
//alert(error);
if(document.getElementById('password').value=="")
{
document.getElementById('passw').innerHTML="password required";
error++;
}

if(document.getElementById('confirmpassword').value=="")
{
document.getElementById('confpassw').innerHTML="reenter password";
error++;
}

/*if(document.getElementById('accept').checked==false)
{
document.getElementById('acceptw').innerHTML="Please agree to our privacy policy";
error++;
}*/

if(validateEmail(document.getElementById('email').value)==false)
{
emailvalid=0;
document.getElementById('emailw').innerHTML="Please enter a valid email address";
error++;
}
else
{
emailvalid=1;
}

if(document.getElementById('password').value!=document.getElementById('confirmpassword').value)
{
document.getElementById('match').innerHTML="Passwords do not match";
error++;
}
else
{
if(document.getElementById('password').value!=""&&document.getElementById('confirmpassword').value!="")
{
document.getElementById('match').style.color="green";
document.getElementById('match').innerHTML="Passwords match";
}
}

if(document.getElementById('password').value.length<5 && document.getElementById('password').value.length!=0)
{
document.getElementById('passw').innerHTML="password length should be greater than 5 chars";
error++;
}

if(Number.isInteger(parseInt(document.getElementById('contact').value))==false||document.getElementById('contact').value.length!=10)
{
document.getElementById('contactw').innerHTML="Please enter a valid mobile no.";
error++;
}

//check_confirmed(document.getElementById('email').value);
//alert(error);
if(error>0)
{
return false;
}

}

function validateEmail(email)
{
 var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
 if (reg.test(email)){
 return true; }
 else{
 return false;
}
}

function check_confirmed(email)
{
var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "1")
                    {
			   document.getElementById('emailw').innerHTML="The email <b>"+document.getElementById('email').value+"</b> has already been registered";
emailOk=0;
                    }
else
{

emailOk=1;
if(emailvalid==1)
{
document.getElementById('emailw').innerHTML="";
}
}

                }
            }
            xmlhttp.open("POST", "cce.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("email="+email);
 }
</script>

    <title>Praxis '14|Register</title>

    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/css/headers/header1.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="assets/css/style_responsive.css" />
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/themes/default.css" id="style_color" />
    <link rel="stylesheet" href="assets/css/themes/headers/default.css" id="style_color-header-1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body style="background-color: #485274;">
<div class="top">
    <div class="container">
        <ul class="loginbar pull-right" style="color: white;" >
<li id='name_tab' class='name_tab'></li>
		 <li id='reg_tab'><a href="#" class="login-btn" style="color: white;">Register</a></li>
            <li class="devider">&nbsp;</li>
            <li id="login_tab"><a href="Login.php" class="login-btn" style="color: white;">Login</a></li>
        </ul>
</div>
</div><!--/top-->
 <div class="header" style="border-bottom: 0">
    <div class="container">
        <!-- Logo -->
        <div class="logo">
            <a href="index.php"><img src="assets2/img/logo.png" style="padding-right:53px; width: 200px; margin-top:-20px"></a>
        </div><!-- /logo -->
<br>
<br><br>
        <!-- End Navbar -->
    </div>
    </div>

<!--=== Content Part ===-->
<div class="body" >
	<div class="breadcrumbs margin-bottom-50" style="background: #343c55; border:none;">
    	<div class="container" >
            <h1 class="color-green pull-left" style="color: white;">Register</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.php" style="color: white;">Home</a> </li>
<li class="active">Registration</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->

	<div class="container" >
    <h1 class="color-green" style="text-align: center;" id="message">Online Treasure Hunt</h1>
		<div class="row-fluid margin-bottom-10" >
        	<form  id="signup" class="reg-page" method="post" action="web_registration.php" onSubmit="if(emailOk==1){return validate();}else{return false;}" style="background-color: #343c55; color: white; padding-right: 3%; padding-left: 3%; border: none;">
          	<h3 style="color: white; background-color: #343c55;">Register a new account</h3>
                <div class="controls" style="background-color: #343c55">
                    <label>First Name</label>
                    <input type="text" class="span12" name="fname" id="fname" onKeyUp="checkcall()" />
<div id='fnamew' class='warning'></div>
                    <label>Last Name</label>
                    <input type="text" class="span12" name="lname" id="lname" onKeyUp="checkcall()" />
<div id='lnamew' class='warning'></div>
                    <label>Email Address <span class="color-red">*</span></label>
                    <input type="text" class="span12" name="email" id="email"  onKeyUp="checkcall();check_confirmed(this.value);" onBlur="check_confirmed(this.value);"/>
<div id='emailw' class='warning'></div>
 		<label>Contact Number(Mobile)<span class="color-red">*</span></label>
                    +91<input type="text" class="span12" name="contact"  id="contact" onKeyUp="checkcall()"/>
<div id='contactw' class='warning'></div>
                </div>
                <div class="controls">
                    <div class="span6">
                        <label>Password <span class="color-red">*</span></label>
                        <input type="password" class="span12" name="password" id="password" onKeyUp="checkcall()"/>
<div id='passw' class='warning'></div>
                    </div>
                    <div class="span6">
                        <label>Confirm Password <span class="color-red" >*</span></label>

                        <input type="password" class="span12" name="confirmpassword" id="confirmpassword" onKeyUp="checkcall()"/>
<div id='confpassw' class='warning'></div>

                    </div>
 <label>College Name/Class Name(for VESITians)</label>
                    <input type="text" class="span12" name="collg-class" id="collg-class"/>
                </div><br/>
<div id='match' class='warning'></div>
                <div class="controls form-inline">
                  <!-- <label class="checkbox"><input type="checkbox" name="accept" id="accept" onClick="checkcall()" />&nbsp; I read <a href="">Terms and Conditions</a></label>-->
<div id='acceptw' class='warning'></div>
                    <button class="btn-u pull-right" type="submit" name="submit" onClick="incr()">Register</button>

                </div>
                <hr />
				<p style="color: white;">Already Signed Up? Click <a href="Login.php" class="color-green">Sign In</a> to login your account.</p>
            </form>

        </div><!--/row-fluid-->
	</div><!--/container-->
</div><!--/body-->
<!--=== End Content Part ===-->

<!--=== Footer ===-->
<div class="footer">
	<div class="container">
		<div class="row-fluid">
			<div class="span4">
                <!-- About -->
		        <div class="headline"><h3>About</h3></div>
				<p class="margin-bottom-25">Praxis is Vivekanand Education Society's Institute of Technology's annual technical festival.</p><br><img style="height:170px;"src="assets/img/ves_logo.png" />
			</div>
			<div class="span4">
                <div class="posts">
                    <div class="headline"><h3>Recent Tech Feeds</h3></div>
                       <script language="JavaScript" src="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y"  charset="UTF-8" type="text/javascript"></script>



<noscript>

<a href="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y&html=y">View RSS feed</a>

</noscript>
                </div>
			</div><!--/span4-->



                 <div class="span4">                <div class="headline"><h3>Contact Us</h3></div>                 <address>                    Vivekanand Education Society's Institute of Technology<br /> Collector Colony,Chembur  <br />                                   </address>
Phone: +91 022 61532532<br/><br/>
Official Website:<a href="http://ves.ac.in/vesit">ves.ac.in/vesit</a>

                <!-- Stay Connected -->
		        <div class="headline"><h3>Stay Connected</h3></div>
                <ul class="social-icons">

                    <li><a target="_blank" href="https://www.facebook.com/praxis.techfest" data-original-title="Facebook" class="social_facebook"></a></li>

                </ul>
			</div><!--/span4-->
		</div><!--/row-fluid-->
	</div><!--/container-->
</div><!--/footer-->
<!--=== End Footer ===-->

<!--=== Copyright ===-->
<div class="copyright">
	<div class="container">
		<div class="row-fluid">
			<div class="span8">
	            <p>2016 &copy; Praxis. ALL Rights Reserved.</p>
			</div>

		</div><!--/row-fluid-->
	</div><!--/container-->
</div><!--/copyright-->
<!--=== End Copyright ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets2/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets2/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets2/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets2/plugins/back-to-top.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets2/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>
<!--[if lt IE 9]>
    <script src="assets/js/respond.js"></script>
<![endif]-->

</body>
</html>
