<?php
session_start();
if(!isset($_SESSION['userid'])) {
  header('location: Login.php');
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<style>
/*
@-webkit-keyframes fader {
    0%   {opacity: 0;}
    50%  {opacity: 1;}
    100% {opacity: 0;}
}


@keyframes fader {
    0%   {opacity: 0;}
    50%  {opacity: 1;}
    100% {opacity: 0;}
}*/

</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
/*function update_all()
{
	update_stats();
	update_ranktable();
	setTimeout(update_all,30000);

}*/
function update_q()
{
var xmlhttp;
document.getElementById("question").innerHTML="<img src='lb_loading.gif'></img><br/><center>Loading Question</center>";
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    //alert(xmlhttp.responseText);
                    document.getElementById("question").innerHTML =xmlhttp.responseText;
                    update_stats();
                    update_ranktable();
                }
            }
            xmlhttp.open("POST", "get_q.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send();
}
function update_stats()
{
var xmlhttp;
//document.getElementById("stats").innerHTML="<img src='lb_loading.gif'></img><br/><center>Loading Stats</center>";
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    //alert(xmlhttp.responseText);
                    document.getElementById("stats").innerHTML =xmlhttp.responseText;
                }
            }
            xmlhttp.open("POST", "get_stats.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send();
}
function update_ranktable()
{
    document.getElementById("ranktable").src ="get_ranktable.php";
}
function check_ans()
{
var xmlhttp;
var ans=document.getElementById("ans").value.trim();
document.getElementById("mess1").innerHTML="<img src='lb_loading.gif'></img>Checking Answer";

            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                    //alert(xmlhttp.responseText);
                    if(xmlhttp.responseText=="1")
                    {
                   	var audio = new Audio('success.mp3');
					audio.play();
				                   //document.getElementById('mess1').style.webkitTransition="opacity 5s";
                    //document.getElementById('mess1').style.opacity="0";
                    document.getElementById("mess1").innerHTML="<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' style='margin-left:-30px;'>x</button><strong>Great that's the right answer!</strong>Answer the next question shown above.</div>";
                    document.getElementById("ans").value="";

                    //document.getElementById('mess1').style.webkitTransition="opacity 5s";
                    //document.getElementById('mess1').style.opacity="1";
                    update_q();                                                     // $("#mess1").fadeIn(4000);
                    }
                    if(xmlhttp.responseText=="01")
                    {
                    	  var audio = new Audio('failure.mp3');
					audio.play();
                                            //document.getElementById('mess1').style.opacity="0";
                        document.getElementById("mess1").innerHTML="<div class='alert alert-error fade in'><button type='button' class='close' data-dismiss='alert' style='margin-left:-30px;'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><strong>Hard luck!</strong>Try again that is not the answer.</div>";
                      //  document.getElementById('mess1').style.webkitAnimation="fader 7s";
                      //document.getElementById('mess1').style.opacity="1";

                    }
                    if(xmlhttp.responseText=="complete")
                    {
                        location.replace('complete101.php');
                    }
                    //document.getElementById("question").innerHTML =xmlhttp.responseText;*/
                }
            }
            xmlhttp.open("POST", "check_answer.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("ans="+ans);
}
</script>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets2/css/style.css">
    <link rel="shortcut icon" href="favicon.ico">
 <link rel="stylesheet" href="assets/css/headers/header1.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="assets/css/style_responsive.css" />
       <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" />
    <link rel="stylesheet" href="assets/css/effects.css" />
   <link rel="stylesheet" href="assets/plugins/revolution_slider/rs-plugin/css/settings.css">
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets2/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets2/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="assets2/plugins/revolution_slider/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="assets2/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets2/css/themes/default.css" id="style_color">
    <link rel="stylesheet" href="assets/css/themes/default.css" id="style_color-header-1">
<style>
.name_tab
{
color:green;
font-weight:bold;
font-family:'Open Sans', sans-serif;
}
</style>

    <title>Praxis'14|OTH Playarea</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->


    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets2/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets2/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets2/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" />

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="assets2/css/pages/portfolio-v1.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets2/css/themes/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets2/css/custom.css">
</head>

<body style="background-color:#485274;" onload="update_q();update_stats();update_ranktable();">
 <div class="top">
    <div class="container">
        <ul class="loginbar pull-right" style="color: white">
<li id='name_tab' class='name_tab' style="color: #2ecc71"></li>
<li class="devider" id='dividern' style="display:none"></li>
         <li id='reg_tab'><a href="index.php" style="color: white" class="login-btn">Register</a></li>
            <li class="devider" style="display:none"></li>
 <li id="uniqueid_tab" class="name_tab" style="color: white"></li>
            <li class="devider" style="display:none">&nbsp;</li>
            <li class="devider">&nbsp;</li>
            <li id="login_tab" style="color: white"><a href="Login.php" class="login-btn">Login</a></li>
        </ul>
</div>
</div><!--/top-->
<?php
require('db_connect2.php');
$query_comp="select current_q from oth_playerdata where email='".$_SESSION['userid']."'";
$row_comp=mysqli_fetch_array(mysqli_query($connection, $query_comp), MYSQLI_ASSOC);
if($row_comp['current_q']==44)
{
   echo "<script>location.replace('complete101.php')</script>";
}
else
{
if(isset($_SESSION['userid']))
{
echo "<script>document.getElementById('login_tab').innerHTML='<a href=\'Logout.php\' style=\'color: white\'>Logout</a>'</script>";

//echo "<script>document.getElementById('event_tab').innerHTML='<a href=\"myEvents.php\">My Events</a>'</script>";

echo "<script>document.getElementById('reg_tab').style.color=\"white\";</script>";

echo "<script>document.getElementById('reg_tab').style.fontWeight=\"bold\";</script>";

echo "<script>document.getElementById('reg_tab').style.fontFamily='\'Open Sans\', sans-serif'</script>";

echo "<script>document.getElementById('reg_tab').innerHTML='".$_SESSION['userid']." '</script>";

$query="Select * from confirmedemails where email='".$_SESSION['userid']."'";

$result=mysqli_query($connection, $query) or die("Could not execute query");

while($row=mysqli_fetch_array($result, MYSQLI_ASSOC))
{
echo "<script>document.getElementById('name_tab').innerHTML='<img src=\'assets/img/user.png\' height=\'28px\' width=\'28px\'><img>".$row['fname']." ".$row['lname']." '</script>";
echo "<script>document.getElementById('dividern').innerHTML='&nbsp;'</script>";
echo "<script>document.getElementById('uniqueid_tab').innerHTML='".$row['uniqueid']." '</script>";

//echo $row['uniqueid'];
}
}
else
{
    echo"<script>window.location.replace('Login.php')</script>";
}
}
?>


    <!--=== Header ===-->
    <div class="header" style="border-bottom: 0">
    <div class="container">
        <!-- Logo -->
        <div class="logo">
            <a href="index.php"><img src="assets2/img/logo.png" style="padding-right:53px; width: 200px; margin-top: -20px"></a>
        </div><!-- /logo -->



      <br>
<br><br>
        <!-- End Navbar -->
    </div>
    </div>

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs margin-bottom-40" style="background: #343c55; border:none;">
    <div class="container">
        <h1 class="color-green pull-left" style="color: white" >Online Treasure Hunt Play Area</h1>
    </div><!--/container-->
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Content Part ===-->
<div class='container'>
<div class='row-fluid'>
<div class='span9'>
<div id="question" style="color: white !important;">
</div>
<br/>
<br/>

<div class="container">
<div class="margin-bottom-20 span9" id="mess1">
        </div>
    <input class="input-xxlarge" type="text" placeholder="Type in your answer" style="height:30px;" id="ans" autocomplete="ofF" onkeydown="if (event.keyCode == 13) document.getElementById('btn').click()">
    <button  class="btn-u btn-u-green" id="btn" type="submit" style="margin-top:-10px;" onclick="check_ans();update_stats();update_ranktable();">Submit Answer</button>
</div>
<br/>
<br/><!--/container-->
</div>

<div class='span3'>
<div class="who margin-bottom-30" id="stats">
            </div>
<div class="who margin-bottom-30" >
<div class='headline' style="color: white;"><h3 style="color: white">Leaderboard</h3></div>
<iframe src="get_ranktable.php" frameborder="0" style="max-width:300px" height="300px" id="ranktable"></iframe>
</div>
<div class="who margin-bottom-30" >
<div class='headline' style="margin-bottom:10px; color: white;"><h3 style="color: white">Hints</h3></div>
<strong style="color: white">Check out our Facebook page</strong><br/>
<a target="_blank" href="https://www.facebook.com/praxis.techfest"><img src="assets/img/facebook-logo.png" style="margin-top:0%;width:10%;height:10%"/></a>
</div>
</div>
</div>
</div>
<!--=== End Content Part ===-->

    <!--=== Footer ===-->
    <div class="footer">    <div class="container">        <div class="row-fluid">            <div class="span4">                <!-- About -->                <div class="headline"><h3>About</h3></div>                  <p class="margin-bottom-25">Praxis is Vivekanand Education Society's Institute of Technology's annual technical festival.</p><br><img style="height:170px;"src="assets/img/ves_logo.png" />            </div>            <div class="span4">                <div class="posts">                    <div class="headline"><h3>Recent Tech Feeds</h3></div>                       <script language="JavaScript" src="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y"  charset="UTF-8" type="text/javascript"></script><noscript><a href="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y&html=y">View RSS feed</a></noscript>                </div>            </div><!--/span4-->            <div class="span4">                <div class="headline"><h3>Contact Us</h3></div>                 <address>                    Vivekanand Education Society's Institute of Technology<br /> Collector Colony,Chembur  <br />                                   </address>
Phone: +91 022 61532532<br/><br/>
Official Website:<a href="http://ves.ac.in/vesit">vesit.edu</a>
               <!-- Stay Connected -->


 <div class="headline"><h2>Stay Connected</h2></div>
                      <a target="_blank" href="https://www.facebook.com/praxis.techfest"><img src="assets/img/facebook-logo.png" style="margin-top:0%;width:10%;height:10%"/></a>
                    <!-- End Social Links -->
                </div>

<!--/span4-->        </div><!--/row-fluid-->     </div><!--/container--> </div><!--/footer-->    <!--=== End Footer ===--><!--=== Copyright ===--><div class="copyright">    <div class="container">        <div class="row-fluid">            <div class="span8">                                     <p>2014 &copy; Praxis. ALL Rights Reserved.</p>            </div>                </div><!--/row-fluid-->    </div><!--/container--> </div><!--/copyright--> <!--=== End Copyright ===-->

<!--=== Copyright ===-->

<!--=== End Copyright ===-->

</div><!--/wrapper-->

<?php
    mysqli_close($connection);
?>

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets2/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets2/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets2/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets2/plugins/back-to-top.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets2/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>
<!--[if lt IE 9]>
    <script src="assets2/plugins/respond.js"></script>
<![endif]-->


</body>
</html>
