<?php
session_start();
require('db_connect.php');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets2/css/style.css">
    <link rel="shortcut icon" href="favicon.ico">        
 <link rel="stylesheet" href="assets/css/headers/header1.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="assets/css/style_responsive.css" />
       <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" />           
    <link rel="stylesheet" href="assets/css/effects.css" />    
   <link rel="stylesheet" href="assets/plugins/revolution_slider/rs-plugin/css/settings.css">
    <!-- CSS Implementing Plugins -->    
    <link rel="stylesheet" href="assets2/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets2/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="assets2/plugins/revolution_slider/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="assets2/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets2/css/themes/default.css" id="style_color">
    <link rel="stylesheet" href="assets2/css/themes/headers/default.css" id="style_color-header-1">    
<style>
.name_tab
{
color:green;
font-weight:bold;
font-family:'Open Sans', sans-serif; 
}

td,th
{
    text-align: center;
}
</style>

    <title>Praxis '14 | Events</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->


    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets2/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets2/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets2/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets2/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" /> 

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets2/css/pages/portfolio-v1.css">
    
    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets2/css/themes/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets2/css/custom.css">
</head>

<script>
function eventDetail(str)
{
if (str=="") {
    document.getElementById("div1").innerHTML="";
    return;
  } 
var xmlhttp;    
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        document.getElementById("div1").innerHTML=xmlhttp.responseText; 
    }
  }
xmlhttp.open("POST","pub.php",true);
xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
xmlhttp.send("evnt="+str);
}
</script>

<body>
<!--=== Header ===-->    
    <div class="header">
    <div class="container"> 
        <!-- Logo -->       
        <div class="logo">                                             
            <a href="index.php"><img src="assets2/img/logo.jpg" style="padding-right:53px"></a>
        </div><!-- /logo -->        
                                 
       
    
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                     <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
      
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse nav-collapse">
                    <ul class="nav navbar-nav">
                 <li><a href="index.php">Home</a></li>
                    
                        
                        <li class="dropdown active">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                Events
                            </a>
                            <ul class="dropdown-menu">
                              
                               
                                <li><a href="software.php">Software</a></li>
                                <li><a href="electronics.php">Electronics</a></li>
                                <li><a href="robotics.php">Robotics</a></li>
                <li><a href="gaming.php">Gaming</a></li>
                                <li><a href="funevents.php">Miscellaneous</a></li>

                            </ul>
                        </li>
                        <li><a href="AboutUs.php">About Us</a></li>
                        <li><a href="Praxis13.php">Praxis '13</a></li>
                        <li><a href="Spo14.php">Sponsors</a></li>
                        
                    </ul>
                </div><!--/navbar-collapse-->
            </div>    
        </div>   
<br>    
<br><br>    
        <!-- End Navbar -->
    </div>
    </div> 

    <div class="container">
        <div class="row-fluid">
            <br/>
            <br/>
            <br/>
            <select class="span12"  onchange="eventDetail(this.value)">
                <option value="">Select event</option>
                <?php
                    $result=mysql_query("SELECT * FROM events") or die("Could not execute query");
                    while($row=mysql_fetch_array($result))
                        echo "<option value='".$row['eventname']."'>".$row['eventname']."</option>";
                ?>
            </select>
            <br/>
            <br/>
            <br/>
            <br/>

            <div id="div1">                
            </div>
            <br/>
            <br/>
            <br/>
        </div>        
    </div>    



<!--=== Footer ===-->
    <div class="footer">    <div class="container">        <div class="row-fluid">            <div class="span4">                <!-- About -->                <div class="headline"><h3>About</h3></div>                  <p class="margin-bottom-25">Praxis is Vivekanand Education Society's Institute of Technology's annual technical festival.</p><br><img style="height:170px;"src="assets/img/ves_logo.png" />            </div>            <div class="span4">                <div class="posts">                    <div class="headline"><h3>Recent Tech Feeds</h3></div>                       <script language="JavaScript" src="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y"  charset="UTF-8" type="text/javascript"></script><noscript><a href="http://feed2js.org//feed2js.php?src=http%3A%2F%2Ffeeds.feedburner.com%2FTechcrunch&num=5&desc=20&utf=y&html=y">View RSS feed</a></noscript>                </div>            </div><!--/span4-->            <div class="span4">                <div class="headline"><h3>Contact Us</h3></div>                 <address>                    Vivekanand Education Society's Institute of Technology<br /> Collector Colony,Chembur  <br />                                   </address> 
Phone: +91 022 61532532<br/><br/>
Official Website:<a href="http://vesit.edu">vesit.edu</a>
               <!-- Stay Connected -->                


 <div class="headline"><h2>Stay Connected</h2></div> 
                      <a target="_blank" href="https://www.facebook.com/Vesit.praxis"><img src="assets/img/facebook-logo.png" style="margin-top:0%;width:10%;height:10%"/></a>
                    <!-- End Social Links -->
                </div>

<!--/span4-->        </div><!--/row-fluid-->     </div><!--/container--> </div><!--/footer-->    <!--=== End Footer ===--><!--=== Copyright ===--><div class="copyright">    <div class="container">        <div class="row-fluid">            <div class="span8">                                     <p>2014 &copy; Praxis. ALL Rights Reserved.</p>            </div>                </div><!--/row-fluid-->    </div><!--/container--> </div><!--/copyright--> <!--=== End Copyright ===-->

<!--=== Copyright ===-->

<!--=== End Copyright ===-->

</div><!--/wrapper-->

<?php
    mysql_close();
?>

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="assets2/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets2/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets2/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->         
<script type="text/javascript" src="assets2/plugins/back-to-top.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets2/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>
<!--[if lt IE 9]>
    <script src="assets2/plugins/respond.js"></script>
<![endif]-->


</body>
</html>