    <div class="row">
   <div class="col-lg-12 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                        <div class="sectionTitle blueBorder">
                            <h1><span>Events</span></h1>
                        </div>
                    </div>

                </div>
                <div class="row">

<div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                        <div class="panel-group eventTab" id="accordion" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"  aria-expanded="false" aria-controls="collapseOne">
                                            <span>01</span> Electronics
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/cafe.png" alt=""/>
                                            </div>
                                            <p>Coffee <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/no.png" alt=""/>
                                            </div>
                                            <p>Martini<br/> from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/wine.png" alt=""/>
                                            </div>
                                            <p>Wine <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/milk.png" alt=""/>
                                            </div>
                                            <p>Coktails from <span>3.99</span></p>
                                        </div>
                                        <div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span>02</span> Robotics
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/cafe.png" alt=""/>
                                            </div>
                                            <p>Coffee <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/no.png" alt=""/>
                                            </div>
                                            <p>Martini <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/wine.png" alt=""/>
                                            </div>
                                            <p>Wine <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/milk.png" alt=""/>
                                            </div>
                                            <p>Coktails from <span>3.99</span></p>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                            <span>03</span> Fun
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/cafe.png" alt=""/>
                                            </div>
                                            <p>Coffee <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/no.png" alt=""/>
                                            </div>
                                            <p>Martini <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/wine.png" alt=""/>
                                            </div>
                                            <p>Wine <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/milk.png" alt=""/>
                                            </div>
                                            <p>Coktails from <span>3.99</span></p>
                                        </div>
                                        <div class="clearfix"></div>
                                     
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <span>04</span> Software
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/cafe.png" alt=""/>
                                            </div>
                                            <p>Coffee <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/no.png" alt=""/>
                                            </div>
                                            <p>Martini <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/wine.png" alt=""/>
                                            </div>
                                            <p>Wine <br/>from <span>3.99</span></p>
                                        </div>
                                        <div class="sCItem">
                                            <div class="sCIImg open">
                                                <img src="images/theClub/milk.png" alt=""/>
                                            </div>
                                            <p>Coktails from <span>3.99</span></p>
                                        </div>
                                        <div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div> 
                    <script>$(".open").colorbox({href:"index.html",maxWidth:'95%', maxHeight:'95%'});</script>