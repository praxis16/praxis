   <html>
    <div class="row">
   <div class="col-lg-12 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                        <div class="sectionTitle blueBorder">
                            <h1><span>Events</span></h1>
                        </div>
                    </div>

                </div>
                <div class="row">

<div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                        <div class="panel-group eventTab" id="accordion" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"  aria-expanded="false" aria-controls="collapseOne">
                                            <span>01</span> Electronics
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <?php
        require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='Electronics'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-bolt fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span> ₹ '.$row["cost"].'</span></p></div>';
        
        }

?>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span>02</span> Robotics
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                                                          <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='Robotics'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-cogs fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <span>03</span>Software
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                                                                                         <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='software'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-code fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?>
                                        <div class="clearfix"></div>
                                     
                                    </div>
                                </div>
                            </div> <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <span>04</span> LAN
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                                                                                                                     <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='LAN Gaming'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-gamepad fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?>
                                        <div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                             <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <span>05</span> VPCS
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                                                                                                                     <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='VPCS'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-camera fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?>
                                        <div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                             <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            <span>06</span> Pre-Praxis
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                                                                                                                     <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='Pre Praxis Events'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-compress fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?>
                                        <div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                             <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                            <span>07</span> Technical
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                                                                                                                                                           <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='Technical, Logic'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-wrench fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?><div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingEight">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                            <span>08</span> Miscellaneous
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                    <div class="panel-body">
                                                                                                                                                                           <?php
        //require("db_connection.php");
        $sql = "SELECT * FROM db_events WHERE category='Miscellaneous'";
        $result = mysql_query($sql,$connection) or die("ERROR 1".mysql_error());
       
        while($row=mysql_fetch_assoc($result))
        {
            $id = $row['event_id'];
            //echo $id;
            echo '<div style="margin-top: 10px" class="sCItem"><div class="sCIImg open" data-value='.$id.'><i class="fa fa-code fa-2x"></i></div>
            <p>'.$row["event_name"].'</br><span > ₹ '.$row["cost"].'</span></p></div>';
        
        }

?><div class="clearfix"></div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div> 
                    <script>$(".open").click(function(){  var url = $(this).attr('data-value');
    
$.colorbox({href:"eventsdata.php?q="+url,maxWidth:'55%', maxHeight:'100%'});})</script>
                    </html>
   