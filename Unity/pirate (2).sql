-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2016 at 06:16 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pirate`
--

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `tid` int(10) NOT NULL,
  `qid` int(10) NOT NULL,
  `url` varchar(200) NOT NULL,
  `gold` int(100) NOT NULL,
  `answer` varchar(1000) NOT NULL DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`tid`, `qid`, `url`, `gold`, `answer`) VALUES
(1, 1, 'http://192.168.0.101/game/Untitled-1.png', 100, '123'),
(2, 2, 'http://192.168.0.101/game/Untitled-1.png', 100, '123'),
(3, 3, 'http://192.168.0.101/game/Untitled-1.png', 1000, '123'),
(4, 4, 'http://192.168.0.101/game/Untitled-1.png', 500, '123');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
`user_id` int(10) NOT NULL,
  `gold` int(20) NOT NULL,
  `spawn` int(11) NOT NULL,
  `count` int(10) NOT NULL,
  `email` varchar(30) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=224 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `user_id`, `gold`, `spawn`, `count`, `email`) VALUES
('123', '123', 123, 10563, 1, 0, NULL),
('12', '12', 222, 125, 1, 1, NULL),
('amey', '123456', 223, 2000, 0, 0, 'amey@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_gold`
--

CREATE TABLE IF NOT EXISTS `user_gold` (
  `user` varchar(20) NOT NULL,
  `looter` varchar(20) NOT NULL,
  `gold` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_gold`
--

INSERT INTO `user_gold` (`user`, `looter`, `gold`) VALUES
(',123', ',0', ''),
(',123', ',0', ''),
(',0', ',0', ''),
(',0', ',0', ''),
(',222', ',123', ''),
(',222', ',123', ''),
('', '', ''),
(',123', ',222', ''),
(',123', ',222', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', ''),
('222', '123', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `question`
--
ALTER TABLE `question`
 ADD UNIQUE KEY `qid` (`qid`), ADD UNIQUE KEY `qid_2` (`qid`), ADD UNIQUE KEY `qid_3` (`qid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=224;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
